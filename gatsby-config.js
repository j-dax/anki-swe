module.exports = {
  siteMetadata: {
    title: "Leitner Stack",
  },
  plugins: ["gatsby-plugin-sass", "gatsby-plugin-gatsby-cloud"],
};
